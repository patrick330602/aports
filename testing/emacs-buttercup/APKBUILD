# Contributor: Celeste <cielesti@protonmail.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=emacs-buttercup
pkgver=1.34
pkgrel=0
_pkgreal=buttercup
pkgdesc="Behavior-Driven Emacs Lisp Testing"
url="https://github.com/jorgenschaefer/emacs-buttercup"
arch="noarch"
license="GPL-3.0-or-later"
depends="bash cmd:emacs"
makedepends="emacs-nox"
source="https://github.com/jorgenschaefer/emacs-buttercup/archive/v$pkgver/emacs-buttercup-$pkgver.tar.gz"

build() {
	emacs -batch -q -no-site-file -L . \
		-eval '(setq byte-compile-error-on-warn t)' \
		-f batch-byte-compile \
		./*.el
}

check() {
	emacs -batch -q -no-site-file -L . \
		-l buttercup -f buttercup-run-discover \
		-- --traceback pretty
}

package() {
	install -Dvm755 bin/buttercup \
		-t "$pkgdir"/usr/bin/

	install -Dvm644 ./*.el ./*.elc \
		-t "$pkgdir"/usr/share/emacs/site-lisp/

	cd "$pkgdir"/usr/share/emacs/site-lisp
	emacs -batch -q -no-site-file \
		-eval "(loaddefs-generate \".\" \"$_pkgreal-loaddefs.el\")"
}

sha512sums="
8aa788b5937ecb89d7a8e486d28df78cb582024c438d6e5ec9ad582b17351b116bca87771a0e8d7386e8357d39f6ca8b109612258e55ca16497f4189eddb7a99  emacs-buttercup-1.34.tar.gz
"
