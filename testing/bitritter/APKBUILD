# Contributor: Clayton Craft <clayton@craftyguy.net>
# Maintainer:
pkgname=bitritter
pkgver=0_git20240328
pkgrel=0
_commit="b8c1c0c952fb53f174be745de0c6cbfa14f14883"
pkgdesc="GTK-based bitwarden client"
url="https://codeberg.org/Chfkch/bitritter"
arch="all !s390x" # 'nix' crate fails to compile
license="MIT"
makedepends="
	cargo
	cargo-auditable
	glib-dev
	graphene-dev
	gtk4.0-dev
	libadwaita-dev
	openssl-dev
"
source="
	$pkgname-$_commit.tar.gz::https://codeberg.org/Chfkch/bitritter/archive/$_commit.tar.gz
	bitritter.desktop
	"
builddir="$srcdir/$pkgname"

export PKG_CONFIG_PATH=/usr/lib/pkgconfig

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	glib_sys_NO_PKG_CONFIG=1 \
		cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/bitritter \
		-t "$pkgdir"/usr/bin/
	install -Dm644 "$srcdir"/bitritter.desktop \
		-t "$pkgdir"/usr/share/applications/
}

sha512sums="
845320c957f0a49f980b291f59a6ced554acca07a55adf8075fecdb8aa3ab3ce99d1b5b9a3598820cf0bdbf9dfce620ef3d0622432781d060206565c542e9b1d  bitritter-b8c1c0c952fb53f174be745de0c6cbfa14f14883.tar.gz
03b31377d37edbaa3cb2447e02caea0ecaad4a0b78b9adb61d0c218af1be5472090b6cc3a8139ddd40a82bc75e826900033ea4225277cdd0f0f77c34543500be  bitritter.desktop
"
