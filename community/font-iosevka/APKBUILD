# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=font-iosevka
pkgver=29.1.0
pkgrel=0
pkgdesc="Versatile typeface for code, from code"
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-aile
	$pkgname-etoile
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-Iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-IosevkaAile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-IosevkaEtoile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-IosevkaSlab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-SGr-IosevkaCurly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-SGr-IosevkaCurlySlab-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base=$pkgver-r$pkgrel
		$pkgname-aile=$pkgver-r$pkgrel
		$pkgname-etoile=$pkgver-r$pkgrel
		$pkgname-slab=$pkgver-r$pkgrel
		$pkgname-curly=$pkgver-r$pkgrel
		$pkgname-curly-slab=$pkgver-r$pkgrel
		"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/Iosevka-*.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/iosevka/IosevkaAile-*.ttc
}

etoile() {
	pkgdesc="$pkgdesc (Iosevka Etoile)"
	amove usr/share/fonts/iosevka/IosevkaEtoile-*.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/IosevkaSlab-*.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/SGr-IosevkaCurly-*.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/SGr-IosevkaCurlySlab-*.ttc
}

sha512sums="
0b75346bc43173a3eaf7951054919049420558ff3cbd4ecbf40c7b3c9a651cb22c3d1395aa4b522732c0f1fa438fafec9481ca8dcfc5fbb5b243de5c7a987e29  PkgTTC-Iosevka-29.1.0.zip
16e5446723efb59eab3788569d71855746d707fa8d6a7d20c9d45bd2101ee1711a60320bb834791518f55832026538f7e9c7f9917c72092059242790df5c880e  PkgTTC-IosevkaAile-29.1.0.zip
31fadbc40dfed9d49b5073322cf17d06a0fb3cf6384ae77b25d3ca3c5fa6b89903e4c404099545bc874cc85968fe9161b9f50da8423db81ab9fe4819e712ad04  PkgTTC-IosevkaEtoile-29.1.0.zip
6251da892a6e04a49c375770fa3bf60bd6ce8ddd3aabe37b406844db788b009bce58fd9a86e2f94fd61cb8dc76f5bf8cc08d33470736336bb646fed122859447  PkgTTC-IosevkaSlab-29.1.0.zip
662752d27dd566284139a110528cd3c004f80c437c878093793e9e7aa82f0872142a716c3fb6c84e95fe1403774148474a76ffaf11344b3553286e67ad76d7fd  PkgTTC-SGr-IosevkaCurly-29.1.0.zip
1986f415edd07f839c7fbb511bcb08e1242223aa254694c7ebdebe95901aa93833d88fecc396bfb87e7eb10d06c8887573698e5f5ae94a86c7964b3a148cf538  PkgTTC-SGr-IosevkaCurlySlab-29.1.0.zip
"
