# Contributor: Thomas Boerger <thomas@webhippie.de>
# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Thomas Boerger <thomas@webhippie.de>
pkgname=hugo
pkgver=0.124.1
pkgrel=0
pkgdesc="Fast and flexible static site generator written in Go"
url="https://gohugo.io/"
license="Apache-2.0"
arch="all !ppc64le !riscv64" # fails tests
makedepends="go"
checkdepends="npm py3-docutils tzdata"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/gohugoio/hugo/archive/v$pkgver/hugo-$pkgver.tar.gz
	skip-para-test.patch
	"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v -o bin/$pkgname --tags extended
	./bin/hugo gen man
	./bin/hugo completion bash > hugo.bash
	./bin/hugo completion fish > hugo.fish
	./bin/hugo completion zsh > hugo.zsh
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/$pkgname "$pkgdir"/usr/bin/$pkgname
	install -Dm644 man/*.1 -t "$pkgdir"/usr/share/man/man1

	install -Dm644 hugo.bash \
		"$pkgdir"/usr/share/bash-completion/completions/hugo
	install -Dm644 hugo.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/hugo.fish
	install -Dm644 hugo.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_hugo
}

sha512sums="
29f51a08f9e757a716c2f7845efc08b1ca3b0bf8e70fd06491735a123ca7da8e169116fa003c147be08d2d5f9752473e9a569085728ec0a9ed7850bd57ba8f93  hugo-0.124.1.tar.gz
6ba192d8cb67f115f7ce596c297a55fc64713a4cdb0077cfbb7e45051c7560f5b668da88f513d4f34d8e0eeb4a9d991c5312d62e454c85e95960d8a33f0f8f69  skip-para-test.patch
"
