# Contributor: Dhruvin Gandhi <contact@dhruvin.dev>
# Maintainer: Dhruvin Gandhi <contact@dhruvin.dev>
pkgname=hledger
pkgver=1.32.2
pkgrel=1
pkgdesc="Command-line interface for the hledger accounting system"
_uipkgdesc="Curses-style terminal interface for the hledger accounting system"
_webpkgdesc="Web interface for the hledger accounting system"
url="https://hledger.org/hledger.html"
_uiurl="https://hledger.org/hledger-ui.html"
_weburl="https://hledger.org/hledger-web.html"
arch="aarch64 x86_64" # limited by ghc
license="GPL-3.0-only"
makedepends="
	ghc
	cabal
	libffi-dev
	ncurses-dev
	zlib-dev
	"
_llvmver=15
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-ui
	$pkgname-ui-doc:uidoc
	$pkgname-web
	$pkgname-web-doc:webdoc
	$pkgname-web-openrc:webopenrc
	"
source="https://github.com/simonmichael/hledger/archive/refs/tags/$pkgver/hledger-$pkgver.tar.gz
	0001-cabal-update-cabal-files.patch
	0002-fix-imp-stack-build-with-ghc-9.8-latest-stackage-nig.patch
	0003-Workaround-incompatibility-with-base64-and-GHC-9.8.patch
	0004-fix-Hide-ambiguous-instance-for.patch

	cabal.project.freeze
	hledger-web.initd
	hledger-web.pre-install"
options="net"

export CABAL_DIR="$srcdir"/cabal
export PATH="/usr/lib/llvm$_llvmver/bin:$PATH"

cabal_update() {
	cd $builddir
	cabal v2-update
	(
		cd "$builddir"
		cabal v2-freeze --shadow-installed-packages
		mv cabal.project.freeze "$startdir/"
	)
}

prepare() {
	default_prepare
	cp "$srcdir"/cabal.project.freeze .
}

build() {
	cabal update
	cabal build --prefix=/usr --enable-relocatable all
}

check() {
	cabal test all
}

package() {
	# hledger
	install -Dm755 "$(cabal list-bin hledger)" "$pkgdir"/usr/bin/hledger
	install -Dm644 hledger/hledger.1 "$pkgdir"/usr/share/man/man1/hledger.1
	install -Dm644 hledger/hledger.info "$pkgdir"/usr/share/info/hledger.info
	install -Dm644 hledger/shell-completion/hledger-completion.bash \
		"$pkgdir"/usr/share/bash-completion/completions/hledger
	# hledger-ui
	install -Dm755 "$(cabal list-bin hledger-ui)" "$pkgdir"/usr/bin/hledger-ui
	install -Dm644 hledger-ui/hledger-ui.1 "$pkgdir"/usr/share/man/man1/hledger-ui.1
	install -Dm644 hledger-ui/hledger-ui.info "$pkgdir"/usr/share/info/hledger-ui.info
	# hledger-web
	install -Dm755 "$(cabal list-bin hledger-web)" "$pkgdir"/usr/bin/hledger-web
	install -Dm644 hledger-web/hledger-web.1 "$pkgdir"/usr/share/man/man1/hledger-web.1
	install -Dm644 hledger-web/hledger-web.info "$pkgdir"/usr/share/info/hledger-web.info
	install -Dm755 "$srcdir"/hledger-web.initd "$pkgdir"/etc/init.d/hledger-web
}

doc() {
	pkgdesc="$pkgdesc (documentation)"
	install_if="docs $pkgname=$pkgver-r$pkgrel"
	amove /usr/share/man/man1/hledger.1 /usr/share/info/hledger.info
	$(command -v pigz || echo gzip) -n -9 "$subpkgdir"/usr/share/man/man1/hledger.1
}

ui() {
	pkgdesc="$_uipkgdesc"
	url="$_uiurl"
	amove /usr/bin/hledger-ui
}

uidoc() {
	pkgdesc="$_uipkgdesc (documentation)"
	url="$_uiurl"
	install_if="docs $pkgname-ui=$pkgver-r$pkgrel"
	amove /usr/share/man/man1/hledger-ui.1 /usr/share/info/hledger-ui.info
	$(command -v pigz || echo gzip) -n -9 "$subpkgdir"/usr/share/man/man1/hledger-ui.1
}

web() {
	pkgdesc="$_webpkgdesc"
	url="$_weburl"
	install="$pkgname-web.pre-install"
	amove /usr/bin/hledger-web
}

webdoc() {
	pkgdesc="$_webpkgdesc (documentation)"
	url="$_weburl"
	install_if="docs $pkgname-web=$pkgver-r$pkgrel"
	amove /usr/share/man/man1/hledger-web.1 /usr/share/info/hledger-web.info
	$(command -v pigz || echo gzip) -n -9 "$subpkgdir"/usr/share/man/man1/hledger-web.1
}

webopenrc() {
	pkgdesc="$_webpkgdesc (OpenRC init scripts)"
	url="$_weburl"
	install_if="openrc $pkgname-web=$pkgver-r$pkgrel"
	amove /etc/init.d/hledger-web
}

sha512sums="
7eaaaa4ddfd435fc0f773785eaee652335f1b2eaf96389c75e9daf8e2a6875a640c93419aa8b008953a15fb0bec54ee03307b5a0f916d8509c2bc84f5059e069  hledger-1.32.2.tar.gz
4a20042b54f55ddd2c9f696b718a4e274a0c0a12bf24876d81399fe14481043b9b69c117bec64f9b11be5e1a26b5ebe070f8700fed25c2417c120d2a7868721e  0001-cabal-update-cabal-files.patch
2e97255b98dab0fbb24fba69cee0c273710a3b1a23f65d8b28b60529da6aa64ca7375ec1c5225be31993cc0fff1e62cab6e7c3bf62948ee245e03a8bb6d7e95e  0002-fix-imp-stack-build-with-ghc-9.8-latest-stackage-nig.patch
4a9df71f109322d362db1a26f4efb952b9823a134eb941ad6443ff533b210cbe4dd76e96c384f02305fa6f0ebeb70f102bd9f7423d600913556fbca6281a44fe  0003-Workaround-incompatibility-with-base64-and-GHC-9.8.patch
f06fd362dac184d48e4fe678bed00ed5240ba5da47cebba683f5c040bd34d51f558cb5d094142acfa162a66b456b4a526507eae954f070b359647629b128aea3  0004-fix-Hide-ambiguous-instance-for.patch
459c717442c39ca7a93ebbe9315995f8b5e3d47ba53d2701c2a8b111b8c34295ca289a4c1ae35987ee7ad4eaea23bddf7f9107e9d99a5031366b8e6c5807398b  cabal.project.freeze
4fd0898b29736c1d7f5b41b1ccca8734f38999e5ba88656e092724a7ce4ed346b4f86d627c99be77934eaa7cd86e33ce20a33a60650f1ad8a527c57387179365  hledger-web.initd
9049869dc413c840928b6868547b0de87c624401eeebbd56a7626744634e6e65f6375ca3cf42e8d307fcc8653fbeaf231dde2b597c482176bbb0ba4034cb2c27  hledger-web.pre-install
"
