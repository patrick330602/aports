# Contributor: Kaarle Ritvanen <kunkku@alpinelinux.org>
# Maintainer: Kaarle Ritvanen <kunkku@alpinelinux.org>
pkgname=py3-phonenumbers
pkgver=8.13.34
pkgrel=1
pkgdesc="International phone number library for Python"
url="https://github.com/daviddrysdale/python-phonenumbers"
arch="noarch"
license="Apache-2.0"
depends="python3"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/p/phonenumbers/phonenumbers-$pkgver.tar.gz"
builddir="$srcdir/phonenumbers-$pkgver"

replaces="py-phonenumbers" # Backwards compatibility
provides="py-phonenumbers=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 testwrapper.py
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
ba841f5c7f71bf8bffc10491a09cb6118915ea6344d4fba805b29a64762298fb0d56f2684812b9b8af8d60777e2af2787718596128b4e73bc5398a5f74128678  phonenumbers-8.13.34.tar.gz
"
